# Code repositories - Dépôts de code  <img src="https://static.cirad.fr/sites/umr-astre.cirad.fr/images/logo.png" align="right">

UMR ASTRE

Cirad - Inrae

https://umr-astre.cirad.fr/


If you are a member of UMR ASTRE you can create __projects__ to host and collaborate on code development, documentation, and research products (articles, slide presentations, books, teaching materials, data-analysis reports, dashboards, visualisations and more). 

You can also leverage the functionalities in the platform for project management (_Issues_) and publishing web sites (_GitLab pages_).

Get started by:

- Send an email to one of the points of contact of UMR ASTRE so they can grant you access to the group.
- Follow a [introductory tutorial](https://forgemia.inra.fr/umr-astre/training/forgemia-tutorial) on `git` and `GitLab`.



### Points of contact - Référents

- Antoni Exbrayat (<antoni.exbrayat@cirad.fr>)

- Facundo Muñoz (<facundo.munoz@cirad.fr>)

